from weather_calculations import weather_calculations


def fileread(year , month = 0 ,
      dir_path = "../weatherfiles/"):

    """
    This module reads the data from the files and put them in appropirate data structures
    """
    
    import fnmatch
    import os 
    
    dir_path = dir_path +'/'

    months = [
            "Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" ,
            "Jul" , "Aug" , "Sept" ,"Oct" , "Nov" , "Dec" 
            ]
            
    #Making a dictionary data structure for the files opted
    
    weather_dict=dict()
    
    for filename in os.listdir(dir_path):
        if fnmatch.fnmatch(filename , "*.txt" ) :
            f = open(dir_path + filename , "r")
            header_line = f.readline()[:-1] #to delete '/n' from last
            
    #only to read the first line of any file to fill the dictionray of weather values with keys names

            for values in header_line.split(",") : 
                if weather_dict.get(values)==None:
                    weather_dict[values]=[]

            f.close()
            break    


    #for matching the files with same name i.e., the same year

    for filename in os.listdir(dir_path):
        flag=False
        
        if month == 0  :

            if fnmatch.fnmatch(filename , "*" + str(year) + "*.txt" ) :

                f=open(dir_path+filename , "r")
                flag=True
               
        
        else :
            if fnmatch.fnmatch(filename , "*" + str(year) + "_" + str(months[month - 1]) + ".txt" ) : 
                                                                     #months[months - 1]-> decreasing one index
                                                                     #and searching from months list
                f=open(dir_path+filename , "r")
                flag=True
                
        keys = list (weather_dict.keys())
        if flag:

            #skipping the header row

            next(f)  
            while True :
                lines = f.readline()[:-1] #to delete '/n' from last
                if not lines : #EOF
                    f.close()
                    break
                lines = lines.split( "," )
                for index , values in enumerate(lines):
                    
                    #adding weather values in proper location of dictionary

                    weather_dict[keys[index]].append(values.strip()) 
            
    
    return weather_dict
                





