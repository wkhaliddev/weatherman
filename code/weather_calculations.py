import statistics
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from termcolor import colored

months = [
            "Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" ,
            "Jul" , "Aug" , "Sept" ,"Oct" , "Nov" , "Dec" 
        ]


def weather_calculations(weather_dict , arg = "-f" ):

    """
    This module will do all the calculations of the required weather readings
    """
    
    if not weather_dict['PKT'] :
            print ("Date not found in the file")
            return


    if arg == "-e" :

        # ​highest temperature​, ​lowest temperature​ and ​humidity
        
        question1(weather_dict)
    
    elif arg == "-a":

        #average highest temperature​, average lowest temperature​,​ average mean humidity​.
        
        question2(weather_dict)
        

    elif arg == "-c" :

        #horizontal bar charts
        
        question3(weather_dict)   

    elif arg == "-f" : 
        
        #Bonus Task
        
        question5(weather_dict)
 
        

def question1(weather_dict):
    
    #1 Higest Temperature : 

    max_value = max([int(i) for i in weather_dict['Max TemperatureC'] if i])
    day = weather_dict["Max TemperatureC"].index(str(max_value)) #finding the index for max temperature 
    day = weather_dict['PKT'][day] #finding day for that max temperature
    
    date = day.split("-")
    date[1] = int (date[1])
    day = str(months[date[1]-1]) + date[2]

    print("max_value" , max_value, "day" , day)

#2 Lowest Temperature : 
    
    min_value = min([int(i) for i in weather_dict['Min TemperatureC'] if i])
    
    day = weather_dict["Min TemperatureC"].index(str(min_value)) #finding the index for max temperature 
    day = weather_dict['PKT'][day] #finding day for that max temperature
    date = day.split("-")
    date[1] = int (date[1])
    day = str(months[date[1]-1]) + date[2]

    
    print("min_value" , min_value, "day" , day)

#3 Humidity : 

    max_value_humidity = max([int (i) for i in weather_dict['Max Humidity'] if i])

    #finding the index for max temperature 

    day = weather_dict["Max Humidity"].index(str(max_value_humidity)) 
    day = weather_dict['PKT'][day] #finding day for that max temperature
    date = day.split("-")
    date[1] = int (date[1])
    day = str(months[date[1]-1]) + date[2] 
    print("max_humidity" , max_value_humidity , "day" , day)

    print("========================================\n")
    
    #Making Report
    c = canvas.Canvas ("Report.pdf")
    c.drawString ( 250 , 650 ,"Highest : "+ str(max_value) + "C on "+str (day))
    c.drawString ( 250 , 600 ,"Lowest : "+ str(min_value) + "C on "+str (day) )
    c.showPage()
    c.save()

def question2(weather_dict):

    sum_value = 0
    count = 0
    
    #1 Average Highest Temperate 

    for i in weather_dict['Max TemperatureC']:
        if i=="":
            continue
        sum_value += int( i )
        count+=1
    avg_highest = sum_value / count

    #2 Average Lowest Temperature

    sum_value = 0
    count = 0

    for i in weather_dict['Min TemperatureC']:
        if i=="":
            continue
        sum_value += int( i )
        count+=1
    avg_lowest = sum_value / count

    #3 Average Mean Humidity

    sum_value = 0
    count = 0
    for i in weather_dict[' Mean Humidity']:
        if i=="":
            continue
        sum_value += int( i )
        count+=1
    avg_mhumidity = sum_value / count

    print ("Highest Average : ", avg_highest, "C")
    print ("Lowest Average : ", avg_lowest, "C")
    print ("Average Mean Humidity : ", avg_mhumidity, "%")

    print("========================================\n")
    
    #Making Report
    c = canvas.Canvas ("Report.pdf")
    c.drawString ( 250 , 650 ,"Highest Average : "+ str(avg_highest) + "C")
    c.drawString ( 250 , 600 ,"Lowest Average : "+ str(avg_lowest) + "C " )
    c.drawString ( 250 , 550 ,"Average Mean Humidity : "+ str(avg_mhumidity))
    c.showPage()
    c.save()





def question3(weather_dict):
    
    #Highest and Lowest temperature on each day
    for index,value in enumerate(weather_dict['Max TemperatureC']):
        if value and weather_dict['Min TemperatureC'][index]: 
            
            #printing accodint to the requirements

            
            print (colored (str (index +1) + " " + "+" *int(value) +value + "C" , 'red'))
            
            #printing according to the question
    
            print (colored (str (index +1) + " " + '+' * int (weather_dict['Min TemperatureC'][index]) + \
                   weather_dict['Min TemperatureC'][index]+"C" , 'blue'))

    print("========================================\n")



def question5(weather_dict):

    #Bonus Question

    for index , value in enumerate(weather_dict['Max TemperatureC']):
        if value and weather_dict['Min TemperatureC'][index]:
            print(index + 1 , "+" * ( int(value) + 
                int(weather_dict['Min TemperatureC'][index]) ) , end ='')

            print (colored (value + "C" , 'red' ) , "-" ,colored ( weather_dict['Min TemperatureC'][index] + "C" , 'blue'))

    print("========================================\n")

