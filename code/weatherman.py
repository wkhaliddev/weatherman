import sys 
from weather_calculations import weather_calculations
from  unzipper import unzip
from fileread import fileread

def main():

    """
    This driver function first uses the uzipper module to extract the weatherfiles into the
    directory, then does the other tasks
    """
    
    
    if len(sys.argv) >= 4 :
        dir_path = sys.argv[1]
        unzip(dir_path)         #uzipping file again and rewritting previous version of copy
    
    else : 
        print( "Arguments not given appropriately" )
        quit()

    # Arguments should always be even. i.e., always a set of two (parameter, year/month)

    if len(sys.argv) % 2 != 0 : 
        print( "Arguments not given appropriately" )
        quit()

    for i in range (2 , len(sys.argv) , 2 ) :  #Making 2 memeber group of arguments i.e., (parameter, year/month)
        
        arg = sys.argv[i]
        date = sys.argv[i+1].split("/")
        if len(date) == 1 :
            year = int (date[0])    #No month here
            month = 0
        else :
            year = int (date[0])
            month = int (date[1])
        
        weather_dict = dict()
        weather_dict = fileread (year , 
                    month , dir_path)   #output of the desired file

        weather_calculations (weather_dict , arg)

    
if __name__=='__main__':

    main()
    