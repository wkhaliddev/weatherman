
def unzip(dir_path):

    """
        This module will check if the zip file is extracted with the same name
        in the same diretory, if it is, then it will replace the files with the 
        new unzip
    """

    import shutil
    from pathlib import Path
    from zipfile import ZipFile

    # removing the old unzip if available
    p = Path(dir_path)

    if p.exists():
        try:
            shutil.rmtree(dir_path)
            #print('Deleting previous unzipped directory')
        except:
            print("Error while deleting directory")
            quit()

    # Extracting the new copy of the weatherfiles zip

    with ZipFile(dir_path+".zip","r") as zip_ref:
        zip_ref.extractall("../")
        
